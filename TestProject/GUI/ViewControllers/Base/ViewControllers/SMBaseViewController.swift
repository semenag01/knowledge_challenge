//
//  SMBaseViewController.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit


public protocol SMBaseViewControllerMakerProtocol { }

extension SMBaseViewControllerMakerProtocol where Self: SMBaseViewController {
    
    static func make(representObject aRepresentObject: Any?) -> Self {
        
        let vc: Self = self.init()
        
        vc.representObject = aRepresentObject
        
        return vc
    }
}

extension SMBaseViewController: SMBaseViewControllerMakerProtocol { }


class SMBaseViewController: SMViewController {
    
    deinit {
        print(#function + " - \(type(of: self))")
    }

    let classNavigationController: SMBaseNavigationController.Type = SMBaseNavigationController.self
    
    var presenter: SMBasePresenter?

    
    // MARK: Represent
    
    var representObject: Any?
    
    func updateRepresentation() {
        
        updateRepresentation(representObject: representObject)
    }
    
    func updateRepresentation(representObject aRepresentObject: Any?) {
        
        presenter?.representObject = aRepresentObject
    }
    

    // MARK: For override
    
    func createPresenter() -> SMBasePresenter? {
        
        return nil
    }
    
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        presenter = createPresenter()
        presenter?.representObject = representObject
        
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if isFirstAppear {

            presenter?.firstViewWillAppear(animated)
        }
        
        presenter?.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        presenter?.viewDidAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
        
        presenter?.viewWillDisappear(animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        presenter?.viewDidDisappear(animated)
    }
    
    override func close() {
        
        super.close()
        
        presenter?.close()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return .lightContent
    }
    

    // MARK: Alert
    
    func showAlertWith(error aError: Error?) {
        
        if let error: Error = aError {
            sm.showAlertController(title: "", message: error.localizedDescription, cancelButtonTitle: "ok".sm.localize())
        }
    }

    func showAlertWith(response aResponse: SMResponse?) {
        
        let titleMessage: String = aResponse?.titleMessage ?? ""
        
        if let textMessage: String = aResponse?.textMessage, let response: SMResponse = aResponse {
            
            if !textMessage.isEmpty && !response.isCancelled {
                
                sm.showAlertController(title: titleMessage, message: textMessage, cancelButtonTitle: "ok".sm.localize())
            }
        }
    }
    
    func showAlertWith(validator aValidator: SMValidator?) {
        
        if let validator: SMValidator = aValidator {
            
            sm.showAlertController(title: validator.titleMessage, message: validator.errorMessage, cancelButtonTitle: "ok".sm.localize())
        }
    }

    func showAlertWith(title aTitle: String?, message aMessage: String?) {
        
        if aTitle != nil || aMessage != nil {
            
            self.sm.showAlertController(title: aTitle, message: aMessage, cancelButtonTitle: "ok".sm.localize())
        }
    }
    
    func showAlertWith(title aTitle: String?, message aMessage: String?, handler aHandler: ((SMAlertController, Int) -> Void)?) {
        
        if aTitle != nil || aMessage != nil {
            
            self.sm.showAlertController(title: aTitle, message: aMessage, cancelButtonTitle: "ok".sm.localize(), otherButtonTitles: nil, handler: aHandler)
        }
    }
    
    
    // MARK: Navigation
    
    func pushViewController(_ viewController: SMBaseViewController) {
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func presentDefault(_ viewController: SMBaseViewController, withAnimation: Bool = true) {
        
        let classNc: UINavigationController.Type = viewController.classNavigationController as UINavigationController.Type
        let nc: UINavigationController = classNc.init(rootViewController: viewController)
        self.present(nc, animated: withAnimation, completion: nil)
    }


    // MARK: - Logic

    func addView(_ aView: UIView, toView: UIView) {

        aView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        aView.frame = toView.bounds
        toView.addSubview(aView)
    }
}
