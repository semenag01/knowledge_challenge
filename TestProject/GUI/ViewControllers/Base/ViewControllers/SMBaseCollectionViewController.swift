//
//  SMBaseCollectionViewController.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit


class SMBaseCollectionViewController: SMBaseListViewController, SMCollectionDisposerDelegate {
    
    var collectionView: UICollectionView? { return listView as? UICollectionView }

    var collectionDisposer: SMCollectionDisposer? { return listDisposer as? SMCollectionDisposer }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    override func createListView() -> UICollectionView {
        
        let collectionViewLayout: UICollectionViewLayout = createCollectionViewLayout()
        configureCollectionViewLayout(collectionViewLayout)
        
        let result: UICollectionView = collectionViewClass().init(frame: self.frameListView(), collectionViewLayout: collectionViewLayout)
        
        return result
    }
    
    func collectionViewClass() -> UICollectionView.Type {
        
        return UICollectionView.self
    }
    
    override func createListDisposer() -> SMListDisposer {
        
        let result: SMCollectionDisposer = SMCollectionDisposer()
                
        return result
    }
    
    override func configureListDisposer() {
        
        super.configureListDisposer()
        
        collectionDisposer?.delegate = self
    }
    

    func createCollectionViewLayout() -> UICollectionViewLayout {
        
        let result: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        return result
    }
    
    func configureCollectionViewLayout(_ aFlowLayout: UICollectionViewLayout) {
        
    }
    
    override func configureListView() {
        
        super.configureListView()        
    }
}
