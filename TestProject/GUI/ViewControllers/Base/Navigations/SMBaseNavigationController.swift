//
//  SMBaseNavigationController.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit


class SMBaseNavigationController: UINavigationController, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        delegate = self
    }
        
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return self.topViewController?.preferredStatusBarStyle ?? .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        
        if let orienation: UIInterfaceOrientationMask = topViewController?.supportedInterfaceOrientations {
            
            return orienation
        }
        
        if SMHelper.isIPad {
            
            return [UIInterfaceOrientationMask.portrait, UIInterfaceOrientationMask.landscape]
        } else {
            
            return [UIInterfaceOrientationMask.portrait]
        }
    }
    
    override var shouldAutorotate: Bool {
        
        let result: Bool = topViewController?.shouldAutorotate ?? true
        
        return result
    }
    
    
    // MARK: UINavigationControllerDelegate
    
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationController.Operation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return transition(from: fromVC, to: toVC)
    }
    
    func transition(from: UIViewController, to: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return nil
    }
}
