//
//  SMStudentsListViewController.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//


import VRGSoftSwiftIOSKit

final class SMStudentsListViewController: SMBaseModelTableViewController {
    
    @IBOutlet weak var vListContainer: UIView!
    @IBOutlet var btsPage: [UIButton]!
    
    override func createPresenter() -> SMBasePresenter {
        
        let result: SMStudentsListPresenter = SMStudentsListPresenter(vc: self)
        
        return result
    }
    
    var custPresenter: SMStudentsListPresenter? {
        
        return presenter as? SMStudentsListPresenter
    }
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        title = "User List"
    }
    
    
    // MARK: Base Overrides
    
    override func parentListView() -> UIView {
        return vListContainer
    }
    

    override func configureListDisposer() {
        
        super.configureListDisposer()
        
        (tableDisposer as? SMTableDisposerModeled)?.register(cellDataClass: SMStudentCellData.self, forModelClass: SMBOStudent.self)
    }
    
    override func defaultSectionForlistAdapter(_ aListAdapter: SMListAdapter) -> SMListSection? {
        
        let section: SMSectionReadonly? = super.defaultSectionForlistAdapter(aListAdapter) as? SMSectionReadonly
        
        let headerView: UIView = UIView(frame: .init(origin: .zero, size: .init(width: view.bounds.width, height: 20.0)))
        headerView.backgroundColor = R.color.orange()
        section?.headerView = headerView
        
        return section
    }
    
    override func tableDisposer(_ aTableDisposer: SMTableDisposer, didCreateCell aCell: UITableViewCell) {
        super.tableDisposer(aTableDisposer, didCreateCell: aCell)
        
        if let cellIndex: Int = aTableDisposer.tableView?.indexPath(for: aCell)?.row {
            aCell.backgroundColor = .red
        }
    }
    
    
    // MARK: Actions
    
    @IBAction func didBtLogOutClicked(_ sender: Any) {
        custPresenter?.logOut()
    }
    
    @IBAction func didBtNextPageClicked(_ sender: Any) {
        custPresenter?.nextOffset()
    }
    
    @IBAction func didBtPageClicked(_ sender: UIButton) {
        custPresenter?.setOffset(sender.tag)
    }
}


// MARK: SMStudentsListPresenterProtocol

extension SMStudentsListViewController: SMStudentsListPresenterProtocol {
    
    func updateOffset(_ offset: Int) {
        let attrs: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 16.0),
            .foregroundColor: R.color.darkGray() as Any,
            .underlineStyle: 0
        ]
        
        btsPage.forEach {
            let title: String = "\($0.tag + 1)"
            let attributedTitle: NSAttributedString = NSAttributedString(string: title, attributes: attrs)
            $0.setAttributedTitle(attributedTitle, for: .normal)
        }
        
        let selectedAttr: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 16.0),
            .foregroundColor: R.color.orange() as Any,
            .underlineStyle: 1
        ]
        
        if let button: UIButton = btsPage.first(where: { $0.tag == offset }) {
            let title: String = "\(button.tag + 1)"
            let attributedTitle: NSAttributedString = NSAttributedString(string: title, attributes: selectedAttr)
            button.setAttributedTitle(attributedTitle, for: .normal)
        }
    }
}
