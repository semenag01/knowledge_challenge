//
//  SMUserCell.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//


import VRGSoftSwiftIOSKit

class SMStudentCellData: SMCellData {
    
    required init(model aModel: AnyObject?) {
        
        super.init(model: aModel)
        
        cellNibName = String(describing: SMStudentCell.self)
        cellHeight = 58.0
    }
}

class SMStudentCell: SMBaseCell {
 
    @IBOutlet weak var lbUserName: UILabel!
    @IBOutlet weak var lbFullName: UILabel!
    @IBOutlet weak var lbGroup: UILabel!

    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
    }
    
    
    // MARK: Base Overrides
    
    override func setupCellData(_ aCellData: SMListCellData) {
        
        super.setupCellData(aCellData)
        
    }
    
    override func setupWith(model aModel: AnyObject?) {
        
        super.setupWith(model: aModel)
        
        guard let model: SMBOStudent = aModel as? SMBOStudent else {
            return
        }
        
        lbUserName.text = model.username
        lbFullName.text = model.fullName
        lbGroup.text = model.group
    }
}
