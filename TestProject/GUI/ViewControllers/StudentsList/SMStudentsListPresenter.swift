//
//  SMStudentsListPresenter.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import VRGSoftSwiftIOSKit

protocol SMStudentsListPresenterProtocol {
    func updateOffset(_ offset: Int)
}

class SMStudentsListPresenter: SMBaseModuleListPresenter {
    
    private var offset: Int = 0 {
        didSet {
            reloadData()
        }
    }
    
    var custViewController: SMStudentsListPresenterProtocol? {
        
        return vc as? SMStudentsListPresenterProtocol
    }
    
    
    // MARK: Base Overrides
    
    override func defaultFetcher() -> SMDataFetcherProtocol? {
        
        return SMFetcherWithRequestBlock { [weak self] _ in
            guard let self: SMStudentsListPresenter = self,
                let userID: String = SMAppManager.shared.session?.userID,
                let token: String = SMAppManager.shared.session?.token else {
                return nil
            }
            
            return SMStudentsGateway.shared.getStudents(userID: userID, token: token, limit: 5, offset: self.offset)
        }
    }
    
    override func reloadData() {
        super.reloadData()
        
        custViewController?.updateOffset(offset)
    }
    
    
    // MARK: Logic
    
    func setOffset(_ offset: Int) {
        self.offset = offset
    }
    
    func nextOffset() {
        if offset < 4 {
            offset += 1
        }
    }
    
    func logOut() {
        apply(request: SMAccountGateway.shared.logOut()) { response in
            if response.isSuccess {
                SMAppManager.shared.updateRootViewController()
            }
        }
    }
}
