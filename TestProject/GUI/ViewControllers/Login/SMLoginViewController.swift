//
//  SMLoginViewController.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit

struct SMUserDefaultsKeys {
    static let isRemberMeEnabled: String = "UserDefaultsIsRemberMeEnabledKey"
    static let storedLogin: String = "UserDefaultsStoredLoginKey"
    static let storedPassword: String = "UserDefaultsStoredPasswordKey"

    static func removeStoredInfo() {

        UserDefaults.standard.set(nil, forKey: storedLogin)
        UserDefaults.standard.set(nil, forKey: storedPassword)
    }

    static func setStoredInfo(login: String, password: String) {

        UserDefaults.standard.set(login, forKey: storedLogin)
        UserDefaults.standard.set(password, forKey: storedPassword)
    }
}

class SMLoginViewController: SMBaseViewController {

    @IBOutlet weak var vEmail: UIView!
    @IBOutlet weak var vPassword: UIView!
    @IBOutlet weak var btLogin: UIButton!
    @IBOutlet weak var btRemberMe: UIButton!

    lazy var vUserNameInputView: SMInputView = {

        let result: SMInputView = SMInputView.make(with: .login)

        result.didEndEditing = SMBlockAction(block: { [weak self] value in

            self?.castPresenter?.inputValues[SMFieldType.login.key] = value as AnyObject
        })

        result.shouldNextClicked = SMBlockAction(block: { [weak self] _ in

            self?.vPasswordInputView.tfValue.becomeFirstResponder()
        })

        return result
    }()

    lazy var vPasswordInputView: SMInputView = {

        let result: SMInputView = SMInputView.make(with: .password)

        result.didEndEditing = SMBlockAction(block: { [weak self] value in

            self?.castPresenter?.inputValues[SMFieldType.password.key] = value as AnyObject
        })

        result.shouldNextClicked = SMBlockAction(block: { [weak self] _ in

            self?.view.endEditing(true)
        })

        return result
    }()

    var isRemberMeEnabled: Bool {

        get {
            UserDefaults.standard.value(forKey: SMUserDefaultsKeys.isRemberMeEnabled) as? Bool ?? false
        } set {
            UserDefaults.standard.set(newValue, forKey: SMUserDefaultsKeys.isRemberMeEnabled)
        }
    }


    // MARK: - Presenter

    override func createPresenter() -> SMBasePresenter? {

        return SMLoginPresenter(vc: self)
    }

    private var castPresenter: SMLoginPresenter? {

        return presenter as? SMLoginPresenter
    }

    
    // MARK: - Lifecycle

    override func viewDidLoad() {

        super.viewDidLoad()

        prepareUI()
        updateRemberMe()
    }


    // MARK: Actions

    @IBAction func didBtRemberMeClicked(_ sender: UIButton) {

        let newValue: Bool = !sender.isSelected
        sender.isSelected = newValue
        isRemberMeEnabled = newValue

        if !isRemberMeEnabled {

            SMUserDefaultsKeys.removeStoredInfo()
        }
    }

    @IBAction func didBtLoginClicked(_ sender: Any) {

        castPresenter?.login()
    }

    // MARK: - Logic

    private func prepareUI() {

        addView(vUserNameInputView, toView: vEmail)
        addView(vPasswordInputView, toView: vPassword)
        btLogin.sm.roundBorder()
        btLogin.shadow()

    }

    func updateRemberMe() {

        btRemberMe.isSelected = isRemberMeEnabled

        if isRemberMeEnabled {
            vUserNameInputView.tfValue.text = UserDefaults.standard.value(forKey: SMUserDefaultsKeys.storedLogin) as? String
            vPasswordInputView.tfValue.text = UserDefaults.standard.value(forKey: SMUserDefaultsKeys.storedPassword) as? String
        }
    }
}
