//
//  SMFieldType.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit

enum SMFieldType {

    case login
    case password

    var placeholder: String {
        switch self {
        case .login:
            return "User Name"
        case .password:
            return "Password"
        }
    }

    var key: String {
        switch self {
        case .login:
            return "email"
        case .password:
            return "password"
        }
    }

    var needSecure: Bool {

        switch self {
        case .login:
            return false
        case .password:
            return true
        }
    }

    var returnKeyType: UIReturnKeyType {
        switch self {
        case .login:
            return .next
        case .password:
            return .done
        }
    }

    var image: UIImage {
        switch self {
        case .login:
            return #imageLiteral(resourceName: "ic_user")
        case .password:
            return #imageLiteral(resourceName: "ic_password")
        }
    }

    var validator: SMValidator {
        switch self {
        case .login:
            return SMValidatorNotEmpty()
        case .password:
            return SMValidatorNotEmpty()
        }
    }
}
