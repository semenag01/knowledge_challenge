//
//  SMInputViewState.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit

enum SMInputViewState {

    case selected
    case inactive
    case notValide

    var tintColor: UIColor {

        switch self {
        case .selected:
            return #colorLiteral(red: 0.9137254902, green: 0.5490196078, blue: 0.231372549, alpha: 1)
        case .inactive:
            return #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
        case .notValide:
            return .red
        }
    }
}
