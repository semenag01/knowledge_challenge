//
//  SMLoginPresenter.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit

protocol SMValidatable {

    func updateValideState()
}

class SMLoginPresenter: SMBasePresenter {

    var inputValues: [String: AnyObject] = [:]


    // MARK: - Requests

    func login() {

        guard checkValide() else {

            (vc as? SMValidatable)?.updateValideState()
            return
        }

        guard let userName: String = inputValues[SMFieldType.login.key] as? String,
            let password: String = inputValues[SMFieldType.password.key] as? String else {

                vc.sm.showAlertController(title: "Warning", message: "Fill all required fields", cancelButtonTitle: "Ok")
                return
        }

        apply(request: SMAccountGateway.shared.login(userName: userName, password: password),
              responseBlock: { response in
                if response.isSuccess {
                    SMUserDefaultsKeys.setStoredInfo(login: userName, password: userName)
                    SMAppManager.shared.updateRootViewController()
                }
        })
    }
}
