//
//  SMInputView.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit

class SMInputView: UIView {

    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var tfValue: SMTextField!

    var didEndEditing: SMBlockAction<String?>?
    var shouldNextClicked: SMBlockAction<Void>?

    var currentState: SMInputViewState = .inactive {

        didSet {

            updateUI()
        }
    }

    override func awakeFromNib() {

        super.awakeFromNib()

        prepareUI()
        tfValue.smdelegate = self
    }

    override func layoutSubviews() {

        super.layoutSubviews()

        sm.roundBorder()
    }

    // MARK: - Logic

    static func make(with aType: SMFieldType) -> SMInputView {

        let result: SMInputView = SMInputView.sm.loadFromNib()

        result.ivImage.image = aType.image.withRenderingMode(.alwaysTemplate)
        result.tfValue.validator = aType.validator
        result.tfValue.placeholder = aType.placeholder
        result.tfValue.returnKeyType = aType.returnKeyType
        result.tfValue.isSecureTextEntry = aType.needSecure

        return result
    }

    private func prepareUI() {

        sm.roundBorder()
        layer.borderWidth = 0.5
    }

    private func updateUI() {

        layer.borderColor = currentState.tintColor.cgColor
        ivImage.tintColor = currentState.tintColor
    }
}


// MARK: - UITextFieldDelegate

extension SMInputView: UITextFieldDelegate {

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {

        guard let text: String = textField.text else {
            return true
        }

        let isValied: Bool = tfValue.validate()
        currentState = isValied ? .inactive : .notValide

        if isValied {
            didEndEditing?.performBlockFrom(sender: text)
        }

        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        shouldNextClicked?.performBlockFrom(sender: ())
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

        currentState = .selected
    }
}
