//
//  SMUIConfigurator.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit


struct kSMColor {
    
}


struct kSMFontName {
    
}


class SMUIConfigurator {
    
    public class navBarItem {
        
        class func back() -> UIBarButtonItem {
            
            let bgI: UIImage? = nil
            
            let button: UIButton = UIButton(type: .system)
            button.setImage(bgI, for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
            button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            button.tintColor = UIColor.white
            button.contentHorizontalAlignment = .center
            
            return UIBarButtonItem(customView: button)
        }
        
        class func close() -> UIBarButtonItem {
            
            let bgI: UIImage? = nil
            
            let button: UIButton = UIButton(type: .system)
            button.setImage(bgI, for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            button.tintColor = UIColor.white
            button.contentHorizontalAlignment = .center

            return UIBarButtonItem(customView: button)
        }
    }
}
