//
//  UIView+Help.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit

extension UIView {

    func shadow(height: CGFloat = 1,
                width: CGFloat = 0,
                color: UIColor = .black,
                opacity: Float = 0.45,
                radius: CGFloat = 8) {

        layer.shadowOffset = CGSize(width: width, height: height)
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
    }
}
