//
//  SMBOReloadProtocol.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import Foundation

protocol SMBOReloadProtocol {
    
    func reloadNotificationKey() -> String
    func sendDidReloadNotification()
}
