//
//  SMAPPConfig.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit

class SMAPPConfig {
    
    private static let baseUrl: String = "https://api.kcdev.pro/"
    private static let versionApi: String = "v2"
    static let baseUrlWithApiVersion: String = baseUrl + versionApi
}
