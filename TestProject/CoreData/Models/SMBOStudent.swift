import VRGSoftSwiftIOSKit
import CoreData

@objc(SMBOStudent)
open class SMBOStudent: _SMBOStudent, SMDBStorableObject {
	
    var fullName: String {
        return (firstName ?? "") + " " + (lastName ?? "")
    }

    public func setupWithDictionary(_ aData: [String: Any], context aContext: NSManagedObjectContext) {
        username = aData["username"] as? String
        firstName = aData["first_name"] as? String
        lastName = aData["last_name"] as? String
        group = aData["group"] as? String
    }
}
