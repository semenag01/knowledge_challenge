import VRGSoftSwiftIOSKit
import CoreData

@objc(SMBOUser)
open class SMBOUser: _SMBOUser, SMDBStorableObject {
	
    public func setupWithDictionary(_ aData: [String: Any], context aContext: NSManagedObjectContext) {
        firstName = aData["first_name"] as? String
        lastName = aData["last_name"] as? String
    }
    
    public static var keyId: String {
        return "account_id"
    }
}
