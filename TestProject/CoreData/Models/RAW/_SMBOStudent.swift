// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SMBOStudent.swift instead.

import Foundation
import CoreData

public enum SMBOStudentAttributes: String {
    case firstName = "firstName"
    case group = "group"
    case identifier = "identifier"
    case lastName = "lastName"
    case username = "username"
}

open class _SMBOStudent: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "SMBOStudent"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<SMBOStudent> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _SMBOStudent.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var firstName: String?

    @NSManaged open
    var group: String?

    @NSManaged open
    var identifier: String?

    @NSManaged open
    var lastName: String?

    @NSManaged open
    var username: String?

    // MARK: - Relationships

}

