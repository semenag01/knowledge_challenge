// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SMBOUser.swift instead.

import Foundation
import CoreData

public enum SMBOUserAttributes: String {
    case firstName = "firstName"
    case identifier = "identifier"
    case lastName = "lastName"
}

open class _SMBOUser: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "SMBOUser"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<SMBOUser> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _SMBOUser.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var firstName: String?

    @NSManaged open
    var identifier: String?

    @NSManaged open
    var lastName: String?

    // MARK: - Relationships

}

