//
//  SMMainStorage.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit

class SMMainStorage: SMDBStorage {
    override func persistentStoreName() -> String? {
        
        return "DataModel"
    }
}
