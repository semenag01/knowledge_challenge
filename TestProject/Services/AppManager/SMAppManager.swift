//
//  SMAppManager.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit


struct SMAppManagerUDKeys {
    
    static let session: String = "SMAppManagerUDKeysSession"
}

class SMAppManager {
    
    static let shared: SMAppManager = SMAppManager()
    
    var session: SMSession?
    
    let route: SMRoute = SMRoute(window: UIApplication.shared.delegate!.window!!) // swiftlint:disable:this force_unwrapping
    
    var isLogined: Bool {

        return (session != nil ? true : false)
    }
    
    init() {
        
        session = UserDefaults.standard.sm.coding(forKey: SMAppManagerUDKeys.session)
        
        updateGateways()
    }
    
    func updateRootViewController() {
        
        if isLogined {
            route.switchToHome()
        } else {
            route.switchToLogin()
        }
    }

    func updateGateways() {
        
        if let token: String = session?.token {
            
            let header: String = "Bearer " + token
            SMGatewayConfigurator.shared.setHTTPHeader(value: header, key: "Authorization")
        } else {
            
            SMGatewayConfigurator.shared.setHTTPHeader(value: nil, key: "Authorization")
        }
    }

    func loginWithSession(_ aSession: SMSession?) {
        
        session = aSession
        
        let ud: UserDefaults = UserDefaults.standard
        
        if let session: SMSession = aSession {
            
            ud.sm.setCoding(session, forKey: SMAppManagerUDKeys.session)
        } else {
            
            ud.removeObject(forKey: SMAppManagerUDKeys.session)
        }
        
        ud.synchronize()
        
        updateGateways()
    }
    
    func logout() {
        
        DispatchQueue.main.async {
            
            self.loginWithSession(nil)
        }
        
        SMDBStorageConfigurator.storage?.clear()
    }
    
    func saveSession() {
        
        if let session: SMSession = session {
            
            let ud: UserDefaults = UserDefaults.standard
            ud.sm.setCoding(session, forKey: SMAppManagerUDKeys.session)
            ud.synchronize()
        }
    }
}

extension SMRoute {

    func switchToLogin() {

        let vc: SMLoginViewController = SMLoginViewController()
        self.switchTo(vc: vc)
    }
    
    func switchToHome() {
        
        let vc: SMStudentsListViewController = SMStudentsListViewController()
        let ncClass: UINavigationController.Type = vc.classNavigationController
        let nc: UINavigationController = ncClass.init(rootViewController: vc)
        self.switchTo(vc: nc)
    }
}

