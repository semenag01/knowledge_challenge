//
//  SMSession.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit


struct SMSessionUDKey {
    static let token: String = "SMSessionKeyToken"
    static let userID: String = "SMSessionKeyUserID"
}


class SMSession: NSObject, NSCoding {
    var userID: String
    let token: String
    
    init(token aToken: String, userID aUserID: String) {
        token = aToken
        userID = aUserID
    }
    
    
    // MARK: - NSCoding
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(token, forKey: SMSessionUDKey.token)
        aCoder.encode(userID, forKey: SMSessionUDKey.userID)
    }
    
    required init?(coder aDecoder: NSCoder) {
        if  let token: String = aDecoder.decodeObject(forKey: SMSessionUDKey.token) as? String,
            let userID: String = aDecoder.decodeObject(forKey: SMSessionUDKey.userID) as? String {
            
            self.token =  token
            self.userID =  userID
        } else {
            return nil
        }
    }
}
