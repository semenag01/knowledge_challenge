//
//  SMAccountGateway.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import VRGSoftSwiftIOSKit
import SwiftyJSON
import Alamofire

class SMAccountGateway: SMBaseGateway {
    
    static let shared: SMAccountGateway = SMAccountGateway()

    func login(userName: String, password: String) -> SMGatewayRequest {
        
        var params: [String: AnyObject] = [:]
        params["username"] = userName as AnyObject
        params["password"] = password as AnyObject
        params["usertype"] = "accountAdmin" as AnyObject
        params["_extend"] = "user" as AnyObject
        
        let request: SMGatewayRequest = self.request(type: .post, path: "auth", parameters: params) { _, response -> SMResponse in
            
            let result: (SMResponse, JSON) = response.defaultResult()
            
            if result.0.isSuccess {
                
                if let object: [String: Any] = result.1.dictionaryObject,
                    let rawResponseData: [String: Any] = object["response"] as? [String: Any],
                    let user: SMBOUser = SMBOUser.makeOrUpdateObjectWithData(rawResponseData["user"]),
                    let token: String = rawResponseData["token"] as? String,
                    let userIdentifier: String = user.identifier {
                    
                    let session: SMSession = SMSession(token: token, userID: userIdentifier)
                    SMAppManager.shared.loginWithSession(session)
                } else {
                    result.0.isSuccess = false
                }
            }
            
            return result.0
        }
        request.parameterEncoding = URLEncoding.default
        return request
    }
    
    func logOut() -> SMGatewayRequest {
        
        var params: [String: AnyObject] = [:]
        params["token"] = SMAppManager.shared.session?.token as AnyObject
        
        let request: SMGatewayRequest = self.request(type: .delete, path: "auth", parameters: params) { _, response -> SMResponse in
            
            let result: (SMResponse, JSON) = response.defaultResult()
            
            if result.0.isSuccess {
                
                SMAppManager.shared.logout()
            }
            
            return result.0
        }
        
        return request
    }
}
