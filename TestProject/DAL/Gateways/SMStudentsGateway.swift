//
//  SMStudentsGateway.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import VRGSoftSwiftIOSKit
import SwiftyJSON

class SMStudentsGateway: SMBaseGateway {

    static let shared: SMStudentsGateway = SMStudentsGateway()

    func getStudents(userID: String, token: String, limit: Int, offset: Int) -> SMGatewayRequest {
        
        var params: [String: AnyObject] = [:]
        params["_limit"] = limit as AnyObject
        params["_start"] = offset as AnyObject
        params["token"] = token as AnyObject
        
        let request: SMGatewayRequest = self.request(type: .get, path: "accounts/\(userID)/students", parameters: params) { _, response -> SMResponse in
            
            let result: (SMResponse, JSON) = response.defaultResult()
            
            if result.0.isSuccess {
                
                if let rawResponse: [Any] = result.1.dictionaryObject?["response"] as? [Any] {
                    
                    result.0.boArray = SMBOStudent.mergeWithDataDefault(rawResponse)
                }
            }
            
            return result.0
        }
        
        return request
    }
}
