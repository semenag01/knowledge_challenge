//
//  AppDelegate.swift
//  TestProject
//
//  Created by Semeniuk Aleksandr on 04/10/2019.
//  Copyright © 2019 OLEKSANDR SEMENUIK. All rights reserved.
//

import UIKit
import VRGSoftSwiftIOSKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        SMGatewayConfigurator.shared.url = URL(string: SMAPPConfig.baseUrlWithApiVersion)
        SMDBStorageConfigurator.registerStorageClass(SMMainStorage.self)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        SMAppManager.shared.updateRootViewController()
        window?.makeKeyAndVisible()

        return true
    }
}
